﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallSpawner : MonoBehaviour
{
    public GameObject dot;
    public Text score;
    int numofBalls;
    
    void Start()
    {
        numofBalls = 0;
            StartCoroutine(Spawner());
    }

    IEnumerator Spawner()
    {
        while (numofBalls <= 250)
        {
        yield return new WaitForSeconds(.25f);
            Spawn();
            numofBalls += 1;
            score.text = numofBalls.ToString();
        }
    }
    void Spawn()
    {

            float spawnY = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
            float spawnX = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);

            Vector2 spawnPosition = new Vector2(spawnX, spawnY);
            Instantiate(dot, spawnPosition, Quaternion.identity);
    }
}
