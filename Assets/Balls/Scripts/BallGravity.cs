﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGravity : MonoBehaviour
{
    public Rigidbody2D rb;

    void Pull(BallGravity pullobj)
    {
        Rigidbody2D rbToPull = pullobj.rb;
        Vector2 dir = rb.position - rbToPull.position;
        float dist = dir.magnitude;
        float forceMag = (rb.mass * pullobj.rb.mass) / Mathf.Pow(dist, 1);
        Vector3 force = dir.normalized * forceMag;
        rbToPull.AddForce(force);
    }
    void FixedUpdate()
    {
        BallGravity[] balls = FindObjectsOfType<BallGravity>();
        foreach (BallGravity ball in balls)
        {
            if (ball != this)
                Pull(ball);
        }
    }

}
