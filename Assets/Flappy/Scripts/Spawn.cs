﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject place;
    public GameObject dotPrefab;
    public Camera mainCamera;
    void SpawnBall()
    {
        StartCoroutine(Wait());

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
                var dotnew = Instantiate(dotPrefab, place.transform);
        dotnew.AddComponent<BallBehaviour>();
        dotnew.GetComponent<BallBehaviour>().SetNewBall();
        mainCamera.GetComponent<SmoothFollow>().player = dotnew.transform;
    }
    void OnEnable()
    {
        EventManager.StartListening(EventManager.BallStop, SpawnBall);
    }

    void OnDisable()
    {
        EventManager.StopListening(EventManager.BallStop, SpawnBall);

    }
}
