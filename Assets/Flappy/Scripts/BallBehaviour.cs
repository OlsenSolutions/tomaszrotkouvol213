﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    public Rigidbody2D rigidBody;
    public Camera shotCamera;
    bool pressedButton = false;
    float release = 0.2f;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        shotCamera = GameObject.FindGameObjectWithTag("Shot").GetComponent<Camera>();

    }

    void OnMouseDown()
    {
        pressedButton = true;
        rigidBody.isKinematic = true;
    }

    void OnMouseUp()
    {
        pressedButton = false;
        rigidBody.isKinematic = false;
        StartCoroutine(ReleaseBall());
    }

    void Update()
    {
        if (pressedButton)
        {
            rigidBody.position = shotCamera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
    IEnumerator ReleaseBall()
    {
        yield return new WaitForSeconds(release);
        GetComponent<SpringJoint2D>().enabled = false;
    }




    void OnCollisionEnter2D(Collision2D col)
    {
        if (!pressedButton)
        {
            rigidBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;

            EventManager.TriggerEvent(EventManager.BallStop);
        }
    }

    public void SetNewBall()
    {
        GetComponent<SpringJoint2D>().connectedAnchor = GameObject.FindGameObjectWithTag("Catapult").GetComponent<Transform>().position;
        shotCamera = GameObject.FindGameObjectWithTag("Shot").GetComponent<Camera>();
    }
}
